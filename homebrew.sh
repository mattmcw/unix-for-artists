#!/bin/bash

echo "Installing Homebrew package manager..."

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
