#!/bin/bash

ffmpeg -i ${1} -c:v libx264 -preset slow -crf 20 -c:a libfdk_aac -vbr 4 -ac 2 -movflags +faststart ${2}
