#!/bin/bash

mkdir vid

echo "Downloading example from archive.org..."
sleep 5
(cd vid && wget -O example.mp4 /
https://archive.org/download/electricsheep-flock-244-32500-6/00244%3D32506%3D22645%3D23651.mp4)

echo "Downloading example vid from youtube..."
sleep 5
(cd vid && youtube-dl /
 --verbose --format 'bestvideo+bestaudio/best' --audio-format best /
 -o youtube_4k https://www.youtube.com/watch?v=7fMk4bAAy3A)

ffmpeg -i ./vid/example.mp4 -vf negate ./vid/example_invert.mp4
#ffmpeg -i ./vid/youtube_4k.mkv -s 1920x1080 -c:v prores_ks -profile:v 3 -t 29 ./vid/youtube_hd.mov