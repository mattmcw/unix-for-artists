#!/bin/bash

ffmpeg -i ${1} -c:v libx264 -preset slow -crf 18 -c:a aac -b:a 192k -pix_fmt yuv420p ${2}
